import * as PIXI from "pixi.js";

import { returnAxisStyle, returnAxisCirclesStyle } from "../styles";

export default class Points extends PIXI.Container {
  constructor(application) {
    super();

    this.AXIS_OFFSET = 30;

    this.app = application.app;
    this.ticker = application.ticker;

    this.xAxis = new PIXI.Graphics();
    this.addChild(this.xAxis);

    this.yAxis = new PIXI.Graphics();
    this.addChild(this.yAxis);

    this.pointsX = [];
    this.pointsY = [];

    this.createPoints();
    this.resize();
  }

  onRunningState() {
    this.visible = true;
    this.resizePoints();
  }

  play() {
    this.onRunningState();
    this.ticker.add(this.animatePoints, this);
  }

  onEndingState() {
    this.visible = false;
    this.ticker.remove(this.animatePoints, this);
  }

  resize() {
    this.drawAxes();
    this.resizePoints();
  }

  startTicker() {
    this.ticker.start();
  }

  drawAxes() {
    this.xAxis
      .clear()
      .lineStyle(returnAxisStyle().width, returnAxisStyle().color)
      .moveTo(this.AXIS_OFFSET, this.app.screen.height - this.AXIS_OFFSET)
      .lineTo(this.app.screen.width, this.app.screen.height - this.AXIS_OFFSET);

    this.yAxis
      .clear()
      .lineStyle(returnAxisStyle().width, returnAxisStyle().color)
      .moveTo(this.AXIS_OFFSET, this.app.screen.height - this.AXIS_OFFSET)
      .lineTo(this.AXIS_OFFSET, 0);
  }

  drawCircle(color, radius) {
    const circle = new PIXI.Graphics();
    circle.lineStyle(0);
    circle.beginFill(color, 1);
    circle.drawCircle(0, 0, radius);
    circle.endFill();
    return circle;
  }

  createPoints() {
    for (let i = 0; i < 10; i++) {
      const xPoint = this.drawCircle(
        returnAxisCirclesStyle().x.color,
        returnAxisCirclesStyle().x.radius
      );
      this.addChild(xPoint);
      this.pointsX.push(xPoint);

      const yPoint = this.drawCircle(
        returnAxisCirclesStyle().y.color,
        returnAxisCirclesStyle().y.radius
      );
      this.addChild(yPoint);
      this.pointsY.push(yPoint);
    }
  }

  resizePoints() {
    this.pointsX.forEach((point, index) => {
      point.x =
        ((this.app.screen.width - this.AXIS_OFFSET) /
          (this.pointsX.length - 1)) *
          index +
        this.AXIS_OFFSET;
      point.y = this.app.screen.height - this.AXIS_OFFSET / 2;
    });

    this.pointsY.forEach((point, index) => {
      point.y =
        ((this.app.screen.height - this.AXIS_OFFSET) / this.pointsY.length) *
          index +
        this.AXIS_OFFSET;
      point.x = this.AXIS_OFFSET / 2;
    });
  }

  animatePoints(delta) {
    this.pointsX.forEach((point) => {
      point.x -= delta * 0.3;
      point.x < this.AXIS_OFFSET &&
        (point.x =
          ((this.app.screen.width - this.AXIS_OFFSET) /
            (this.pointsX.length - 1)) *
            (this.pointsY.length - 1) +
          this.AXIS_OFFSET);
    });

    this.pointsY.forEach((point) => {
      point.y += delta * 0.3;
      point.y > this.app.screen.height - this.AXIS_OFFSET && (point.y = 0);
    });
  }
}
