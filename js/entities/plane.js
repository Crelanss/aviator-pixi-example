import * as PIXI from "pixi.js";
import { gsap } from "gsap";
import { PixiPlugin } from "gsap/PixiPlugin";
import { SmoothGraphics as Graphics } from "@pixi/graphics-smooth";

import {
  returnPlaneStyle,
  returnPlaneFillStyle,
  returnPlaneLineStyle,
} from "../styles";

gsap.registerPlugin(PixiPlugin);

export default class Plane extends PIXI.Container {
  constructor(application, visualModel, axes) {
    super();

    this.axes = axes;
    this.endPoint = { x: 0, y: 0 };
    this.controlPoint = { x: 0, y: 0 };
    this.mountPoint = { x: 0, y: 0 };
    this.glidePoint = { x: 0, y: 0 };
    this.startPoint = { x: 0, y: 0 };
    this.CONTROL_POINT_X_PERCENT = 0.6;
    this.MOUNT_TIME = 8;
    this.GLIDE_TIME = 4;
    this.MOUNT_POINT_OFFSET_PLANE_SCALE = { x: 1.5, y: 1 };
    this.GLIDE_POINT_OFFSET_PLANE_SCALE = { x: 1, y: 3 };

    this.app = application.app;

    const planeFrames = [];

    visualModel.forEach((frame) => planeFrames.push(frame.texture));

    this.planeSprite = new PIXI.AnimatedSprite(planeFrames);
    this.planeSprite.anchor.set(
      returnPlaneStyle().anchorX,
      returnPlaneStyle().anchorY
    );
    this.addChild(this.planeSprite);
    this.planeSprite.animationSpeed = 0.125;
    this.planeSprite.play();

    this.planeLine = new Graphics();
    this.addChild(this.planeLine);

    this.planeFill = new Graphics();
    this.planeFill.lineStyle(0, returnPlaneFillStyle().color);
    this.addChild(this.planeFill);

    this.draw = () => {
      this.planeSprite.x = this.endPoint.x;
      this.planeSprite.y = this.endPoint.y;
      this.controlPoint.x = this.endPoint.x * this.CONTROL_POINT_X_PERCENT;

      this.planeLine
        .clear()
        .lineStyle(returnPlaneLineStyle().width, returnPlaneLineStyle().color)
        .moveTo(
          this.startPoint.x,
          this.startPoint.y - returnPlaneLineStyle().width / 2
        )
        .quadraticCurveTo(
          this.controlPoint.x,
          this.controlPoint.y,
          this.planeSprite.x + this.planeSprite.width * 0.1,
          this.planeSprite.y
        );

      this.planeFill
        .clear()
        .beginFill(returnPlaneFillStyle().color, returnPlaneFillStyle().alpha)
        .moveTo(this.startPoint.x, this.startPoint.y)
        .quadraticCurveTo(
          this.controlPoint.x,
          this.controlPoint.y,
          this.planeSprite.x + this.planeSprite.width * 0.1,
          this.planeSprite.y
        )
        .lineTo(
          this.planeSprite.x + this.planeSprite.width * 0.1,
          this.startPoint.y
        );
    };

    this.timeLineMount = gsap.timeline({
      onUpdate: this.draw,
    });
    this.timeLineMain = gsap.timeline({
      onUpdate: this.draw,
    });

    this.resize();
  }

  clearShape() {
    this.planeFill.clear();
    this.planeLine.clear();
    this.timeLineMain.clear();
    this.endPoint.x = this.startPoint.x;
    this.endPoint.y = this.startPoint.y;
  }

  reset() {
    this.planeSprite.x = this.startPoint.x;
    this.planeSprite.y = this.startPoint.y;
    this.clearShape();
    this.planeSprite.visible = false;
  }

  onRunningState() {
    this.playTimeLine(0);
  }

  onBettingState() {
    this.reset();
    this.calculatePlaneScale();
    this.planeSprite.visible = true;
  }

  onEndingState() {
    this.flyAway();
  }

  flyAway() {
    this.clearShape();
    const flyAwayPosition = {
      x: this.app.screen.width + this.planeSprite.getLocalBounds().width,
      y: this.planeSprite.y - 200,
      ease: "none",
    };

    gsap.to(this.planeSprite, { ...flyAwayPosition, duration: 1 }).then(() => {
      this.planeSprite.visible = false;
    });
  }

  resize() {
    this.calculatePlaneScale();
    this.startPoint.x = this.axes.AXIS_OFFSET;
    this.startPoint.y = this.app.screen.height - this.axes.AXIS_OFFSET;
    this.planeSprite.x = this.startPoint.x;
    this.planeSprite.y = this.startPoint.y;
    this.endPoint.x = this.startPoint.x;
    this.endPoint.y = this.startPoint.y;
    this.controlPoint.y = this.startPoint.y;
    this.mountPoint.x = Math.max(
      this.app.screen.width -
        this.MOUNT_POINT_OFFSET_PLANE_SCALE.x * this.planeSprite.width,
      this.startPoint.x
    );
    this.mountPoint.y = Math.min(
      this.MOUNT_POINT_OFFSET_PLANE_SCALE.y * this.planeSprite.height,
      this.startPoint.y
    );
    this.glidePoint.x = Math.max(
      this.app.screen.width -
        this.GLIDE_POINT_OFFSET_PLANE_SCALE.x * this.planeSprite.width,
      this.startPoint.x
    );
    this.glidePoint.y = Math.min(
      this.GLIDE_POINT_OFFSET_PLANE_SCALE.y * this.planeSprite.height,
      this.startPoint.y
    );
  }

  playTimeLine(t) {
    const e = t ? t / 1e3 : this.timeLineMain.time();
    this.reset();
    this.planeSprite.visible = true;
    this.timeLineMount
      .clear()
      .to(
        this.endPoint,
        {
          x: this.mountPoint.x,
          ease: "Power1.easeOut",
          duration: this.MOUNT_TIME,
        },
        0
      )
      .to(
        this.endPoint,
        {
          y: this.mountPoint.y,
          ease: "Power1.easeInOut",
          duration: this.MOUNT_TIME,
        },
        0
      )
      .eventCallback("onComplete", () => this.axes.play());
    this.timeLineMain
      .clear()
      .add(this.timeLineMount)
      .to(this.endPoint, {
        x: this.glidePoint.x,
        y: this.glidePoint.y,
        ease: "Power1.easeInOut",
        repeat: -1,
        yoyo: !0,
        duration: this.GLIDE_TIME,
      })
      .time(e);
  }

  calculatePlaneScale() {
    let t = this.app.screen.width / 900;
    t < 1 ? (t = Math.max(0.65, t)) : t > 1 && (t = Math.min(1, t));
    this.planeSprite.width = this.planeSprite.getLocalBounds().width * t;
    this.planeSprite.height = this.planeSprite.getLocalBounds().height * t;
  }
}
