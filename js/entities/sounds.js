import { sound } from "@pixi/sound";

export default class Sounds {
  constructor(audioModel) {
    this.audioModel = audioModel;
    this.sound = sound;

    this.sprites = {
      onEndingState: { start: 2, end: 4 },
      onRunningState: { start: 6, end: 7 },
    };

    this.sound.add("musicSprites", {
      url: this.audioModel.musicSprites,
      sprites: this.sprites,
    });
    this.sound.add("bgMusic", { url: this.audioModel.bgMusic, loop: true });
  }

  onEndingState() {
    this.sound.play("musicSprites", "onEndingState");
  }

  onRunningState() {
    this.sound.play("musicSprites", "onRunningState");
  }

  playBackground() {
    this.sound.play("bgMusic");
  }
}
