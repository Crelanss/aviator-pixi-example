import * as PIXI from "pixi.js";

import { returnCoeffTextStyle, returnFlewAwayTextStyle } from "../styles";
import * as render from "../render";

export default class Text extends PIXI.Container {
  constructor(application, isCrush) {
    super();

    this.isCrush = isCrush;
    this.app = application.app;

    this.coeffText = new PIXI.Text(
      "0",
      returnCoeffTextStyle(this.isCrush, true).textStyle
    );
    this.coeffText.anchor.set(
      returnCoeffTextStyle(this.isCrush, true).anchor.x,
      returnCoeffTextStyle(this.isCrush, true).anchor.y
    );
    this.coeffText.x = this.app.screen.width / 2;
    this.coeffText.y = this.app.screen.height / 2;
    this.addChild(this.coeffText);

    this.xText = new PIXI.Text(
      "x",
      returnCoeffTextStyle(this.isCrush).textStyle
    );
    this.xText.anchor.set(
      returnCoeffTextStyle(this.isCrush).anchor.x,
      returnCoeffTextStyle(this.isCrush).anchor.y
    );
    this.xText.x = this.app.screen.width / 2 + this.coeffText.width;
    this.xText.y = this.app.screen.height / 2;
    this.addChild(this.xText);

    this.flewAwayText = new PIXI.Text(
      "FLEW AWAY!",
      returnFlewAwayTextStyle().textStyle
    );
    this.flewAwayText.anchor.set(
      returnFlewAwayTextStyle().anchor.x,
      returnFlewAwayTextStyle().anchor.y
    );
    this.flewAwayText.x = this.app.screen.width / 2;
    this.flewAwayText.y =
      this.app.screen.height / 2 - this.coeffText.height - 10;
    this.addChild(this.flewAwayText);
  }

  onRunningState(tick) {
    this.setCrush(false);
    this.flewAwayText.visible = false;
    this.visible = true;
    this.updateCoeff(tick);
    this.moveTextToCenter();
    this.changeTextStyle();
  }

  onEndingState() {
    this.setCrush(true);
    this.changeTextStyle();
    this.flewAwayText.visible = true;
  }

  onBettingState() {
    this.visible = false;
    this.flewAwayText.visible = false;
  }

  updateCoeff(tick) {
    this.coeffText.text = tick.toFixed(2);
  }

  setCrush(isCrush) {
    this.isCrush = isCrush;
  }

  changeTextStyle() {
    this.coeffText.style = returnCoeffTextStyle(this.isCrush, true).textStyle;
    this.xText.style = returnCoeffTextStyle(this.isCrush, false).textStyle;
  }

  moveTextToCenter() {
    this.xText.x = this.app.screen.width / 2 + this.coeffText.width / 2;
    this.coeffText.x = this.app.screen.width / 2 - this.xText.width / 2;
  }

  /* increment() {
    if (this.tick.toFixed(2) === this.crashCoeff.toFixed(2)) {
      this.isCrush = true;
      this.displayFlewAwayText();
      this.changeTextStyle();
    }
    this.tick += 0.001;
    render.renderCrushText(
      this.crashCoeff,
      this.tick,
      this.coeffText,
      this.isCrush,
      (crashCoeff) => {
        this.tick = crashCoeff;
        this.app.stage.emit("crush");
      }
    );
    this.moveTextToCenter();
  } */
}
