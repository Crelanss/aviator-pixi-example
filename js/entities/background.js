import * as PIXI from "pixi.js";

import { returnBackgroundStyle } from "../styles";
import * as render from "../render";

export default class Background extends PIXI.Container {
  constructor(application, visualModel) {
    super();

    this.app = application.app;
    this.ticker = application.ticker;

    this.backgroundSprite = new PIXI.Sprite(visualModel.texture);
    this.backgroundSprite.anchor.set(
      returnBackgroundStyle().anchorX,
      returnBackgroundStyle().anchorY
    );
    this.backgroundSprite.width = returnBackgroundStyle().width;
    this.backgroundSprite.height = returnBackgroundStyle().height;
    this.backgroundSprite.x = 0;
    this.backgroundSprite.y = this.app.screen.height;
    this.addChild(this.backgroundSprite);

    this.ticker.add((delta) => this.rotate(delta));
  }

  onRunningState() {
    this.play();
  }

  onEndingState() {
    this.stop();
  }

  play() {
    this.ticker.start();
  }

  stop() {
    this.ticker.stop();
  }

  rotate(delta) {
    render.renderBackground(delta, 0.005, this.backgroundSprite);
  }
}
