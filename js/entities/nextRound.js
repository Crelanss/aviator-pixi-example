import * as PIXI from "pixi.js";
import { gsap } from "gsap";

import { returnNextRoundStyle } from "../styles";

export default class NextRound extends PIXI.Container {
  constructor(application, visualModel) {
    super();

    this.app = application.app;

    this.size = "lg";

    this.bettingStateDuration = null;

    this.propellerSprite = new PIXI.Sprite(visualModel.texture);
    this.propellerSprite.anchor.set(0.5);
    this.addChild(this.propellerSprite);

    this.waitingText = new PIXI.Text(
      "WAITING FOR NEXT ROUND",
      new PIXI.TextStyle(returnNextRoundStyle(this.size).text)
    );
    this.waitingText.x = 100;
    this.waitingText.y = 500;
    this.addChild(this.waitingText);

    this.timer = new PIXI.Graphics();
    this.timer
      .beginFill(returnNextRoundStyle(this.size).parameters.timerColor)
      .drawRoundedRect(
        0,
        0,
        returnNextRoundStyle(this.size).parameters.timerWidth,
        returnNextRoundStyle(this.size).parameters.timerHeight,
        returnNextRoundStyle(this.size).parameters.timerRadius
      )
      .endFill();
    this.addChild(this.timer);

    this.timerBar = new PIXI.Graphics();
    this.timerBar
      .beginFill(returnNextRoundStyle(this.size).parameters.timerBarColor)
      .drawRoundedRect(
        0,
        0,
        returnNextRoundStyle(this.size).parameters.timerWidth,
        returnNextRoundStyle(this.size).parameters.timerHeight,
        returnNextRoundStyle(this.size).parameters.timerRadius
      )
      .endFill();
    this.addChild(this.timerBar);

    this.timerBarTimeLine = gsap.timeline({});
    this.timerBarTimeLine
      .to(this.timerBar, {
        width: 0,
        ease: "none",
      })
      .duration(0);

    this.propellerSpriteTimeLine = gsap.timeline({});
    this.propellerSpriteTimeLine.to(this.propellerSprite, {
      rotation: 360,
      repeat: -1,
      ease: "none",
      duration: 140,
    });

    this.resize();
  }

  onBettingState(timeLeft, fullbetTime) {
    clearTimeout(this.bettingStateDuration);

    this.visible = true;

    this.propellerSpriteTimeLine.resume();

    this.timerBarTimeLine
      /* .pause((fullbetTime - timeLeft) / 1e3) */
      .duration(timeLeft / 1e3)
      .play();

    /* new Promise(() => {
      this.bettingStateDuration = setTimeout(() => {
        this.onRunningState();
      }, timeLeft);
    }); */
  }

  onRunningState() {
    this.visible = false;
    this.propellerSpriteTimeLine.pause();
    this.timerBarTimeLine.pause(0);
  }

  resize() {
    this.propellerSprite.width = returnNextRoundStyle(
      this.size
    ).parameters.propellerSize;
    this.propellerSprite.height = returnNextRoundStyle(
      this.size
    ).parameters.propellerSize;
    this.propellerSprite.x = this.app.screen.width / 2;
    this.propellerSprite.y =
      this.app.screen.height / 2 -
      (returnNextRoundStyle(this.size).parameters.textMarginTop +
        this.waitingText.height +
        returnNextRoundStyle(this.size).parameters.timerMarginTop +
        this.timer.height) /
        2;

    this.waitingText.style.fontSize = returnNextRoundStyle(
      this.size
    ).text.fontSize;
    this.waitingText.x = this.app.screen.width / 2 - this.waitingText.width / 2;
    this.waitingText.y =
      this.propellerSprite.y +
      this.propellerSprite.height / 2 +
      returnNextRoundStyle(this.size).parameters.textMarginTop;

    this.timer.x = this.app.screen.width / 2 - this.timer.width / 2;
    this.timer.y =
      this.waitingText.y +
      this.waitingText.height +
      returnNextRoundStyle(this.size).parameters.timerMarginTop;

    this.timerBar.x = this.timer.x;
    this.timerBar.y = this.timer.y;
  }
}
