import * as PIXI from "pixi.js";
window.PIXI = PIXI;

export class Application {
  constructor() {
    this.app = new PIXI.Application({
      width: window.innerWidth,
      height: window.innerHeight,
      backgroundColor: 0x040404,
      resolution: window.devicePixelRatio,
      autoDensity: true,
    });

    document.body.appendChild(this.app.view);

    this.ticker = new PIXI.Ticker();
    this.ticker.start();
  }
}

const app = (window.app = new Application());
