const app = window.app;

class AuidoModles {
  constructor() {
    app.app.stage.on("loaded", () => {
      this.createModels();
    });
  }

  createModels() {
    const resources = app.loader.resources;
    const bgMusic = resources.bgMusic.sound;
    const musicSprites = resources.musicSprites.sound;

    Object.assign(this, {
      sounds: {
        bgMusic: bgMusic,
        musicSprites: musicSprites,
      },
    });
  }
}

app.audio = new AuidoModles();
