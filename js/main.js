import "regenerator-runtime/runtime";

import "./app.js";
import Aviator from "./aviator.js";

const app = window.app;

const aviator = new Aviator(app);
app.app.stage.addChild(aviator);
