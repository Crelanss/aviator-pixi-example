import * as PIXI from "pixi.js";

import FontFaceObserver from "fontfaceobserver";
import Background from "./entities/background";
import Plane from "./entities/plane";
import Text from "./entities/text";
import Points from "./entities/points";
import NextRound from "./entities/nextRound";
import Sounds from "./entities/sounds";
import "./resources.js";
import "./visual.js";
import "./audio.js";

const robotoBoldFont = new FontFaceObserver("Roboto Custom Bold");
const alkSanetFont = new FontFaceObserver("Alk Sanet");

export default class Aviator extends PIXI.Container {
  constructor(app) {
    super();

    this.background = null;
    this.points = null;
    this.plane = null;
    this.text = null;
    this.nextRound = null;
    this.sounds = null;

    this.ticker = app.app.ticker;

    this.isCrush = false;

    this.crashCoeff = 1.4;

    this.tick = 0;

    app.app.stage.on("loaded", async () => {
      await this.createScene();
      this.sounds.playBackground();
      this.onRunningState();
    });
  }

  async createScene() {
    this.sounds = new Sounds(app.audio.sounds);

    this.background = new Background(app, app.visual.background);
    this.addChild(this.background);

    this.points = new Points(app);
    this.addChild(this.points);

    this.plane = new Plane(app, app.visual.plane, this.points);
    this.addChild(this.plane);

    await Promise.all([robotoBoldFont.load(), alkSanetFont.load()]).then(() => {
      this.text = new Text(app, this.isCrush);
      this.addChild(this.text);
      this.nextRound = new NextRound(app, app.visual.propeller);
      this.addChild(this.nextRound);
    });
  }

  onRunningState() {
    this.ticker.add(this.count, this);
    this.nextRound.onRunningState();
    this.plane.onRunningState();
    this.background.onRunningState();
    this.points.onRunningState();
    this.sounds.onRunningState();
  }

  onEndingState() {
    this.text.onEndingState();
    this.points.onEndingState();
    this.plane.onEndingState();
    this.background.onEndingState();
    this.sounds.onEndingState();

    setTimeout(() => {
      //говнокодим таймауты пока не интегрируем с фронтом и бэком
      this.onBettingState(2000, 4000);
    }, 2000);
  }

  onBettingState(timeLeft, fullbetTime) {
    this.nextRound.onBettingState(timeLeft, fullbetTime);
    this.text.onBettingState();
    this.plane.onBettingState();

    setTimeout(() => {
      this.onRunningState();
    }, 2000);
  }

  count() {
    if (this.tick.toFixed(2) === this.crashCoeff.toFixed(2)) {
      this.onEndingState();
      this.tick = 0;
      this.ticker.remove(this.count, this);
    } else {
      this.tick += 0.001;
      this.text.onRunningState(this.tick); // костыль пока не будем получать текущий коэфф извне
    }
  }
}
