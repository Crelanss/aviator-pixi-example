import { TextStyle } from "pixi.js";

export const returnCoeffTextStyle = (isCrush) => {
  let fill = ["#ffffff"];

  if (isCrush) {
    fill = ["#d10014"];
  }

  return {
    textStyle: new TextStyle({
      fontFamily: "Roboto Custom Bold",
      fontSize: 90,
      fontWeight: "bold",
      fill: fill,
    }),
    anchor: {
      x: 0.5,
      y: 1,
    },
  };
};

export const returnFlewAwayTextStyle = () => {
  return {
    textStyle: new TextStyle({
      fontFamily: "Alk Sanet",
      fontSize: 30,
      fontWeight: "normal",
      fill: ["#ffffff"],
    }),
    anchor: {
      x: 0.5,
      y: 0.5,
    },
  };
};

export const returnPlaneStyle = () => {
  return {
    width: 150,
    height: 74,
    anchorX: 0,
    anchorY: 1,
  };
};

export const returnBackgroundStyle = () => {
  return {
    width: 5000,
    height: 5000,
    anchorX: 0.5,
    anchorY: 0.5,
  };
};

export const returnPlaneLineStyle = () => {
  return {
    width: 4,
    color: 0xf0033c,
  };
};

export const returnPlaneFillStyle = () => {
  return {
    color: 0xf0033c,
    alpha: 0.5,
  };
};

export const returnAxisStyle = () => {
  return {
    color: 0x423033,
    width: 1,
  };
};

export const returnAxisCirclesStyle = () => {
  return {
    x: {
      color: 0xffffff,
      radius: 2,
    },
    y: {
      color: 0x1197d6,
      radius: 2,
    },
  };
};

export const returnNextRoundStyle = (size) => {
  let propellerSize;
  let textSize;

  switch (size) {
    case "sm":
      propellerSize = 50;
      textSize = 20;
      break;
    case "md":
      propellerSize = 90;
      textSize = 26;
      break;
    case "lg":
      propellerSize = 120;
      textSize = 36;
      break;
    default:
      propellerSize = 90;
      textSize = 26;
      break;
  }
  return {
    parameters: {
      propellerSize: propellerSize,
      textMarginTop: 10,
      timerHeight: 6,
      timerWidth: 200,
      timerRadius: 4,
      timerColor: 0x262830,
      timerBarColor: 0xe50539,
      timerMarginTop: 16,
    },
    text: {
      fontFamily: "Alk Sanet",
      fill: 0xffffff,
      fontSize: textSize,
    },
  };
};
