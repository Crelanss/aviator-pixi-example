const calculateQuadraticCurve = (line, container, sprite, curveness) => {
  line.quadraticCurveTo(
    sprite.x * curveness,
    container.height,
    sprite.x + 17,
    sprite.y - 7
  );
};

const calculateExponent = (endX, endY, height) => {
  return Math.pow(height - endY, 1 / endX);
};

const stopPlane = (sprite, endX) => {
  if (sprite.x >= endX) {
    return true;
  }
  return false;
};

export const returnIsCrush = (crashCoeff, tick) => {
  if (crashCoeff.toFixed(2) === tick.toFixed(2)) {
    return true;
  }
  return false;
};

export const renderPlaneLine = (
  line,
  width,
  color,
  container,
  sprite,
  curveness
) => {
  line.clear();
  line.lineStyle(width, color);

  line.moveTo(0, container.height - width / 2);

  calculateQuadraticCurve(line, container, sprite, curveness);
};

export const renderFillPlaneLine = (
  line,
  fillColor,
  fillAlpha,
  container,
  sprite,
  curveness
) => {
  line.clear();
  line.lineStyle(0, 0x000000);

  line.beginFill(fillColor, fillAlpha);

  line.moveTo(0, container.height);
  calculateQuadraticCurve(line, container, sprite, curveness);
  line.lineTo(sprite.x + 17, container.height);

  line.endFill();
};

export const renderPlane = (sprite, container, tick, endX, endY) => {
  if (stopPlane(sprite, endX)) {
    sprite.y = sprite.y;
    sprite.x = sprite.x;
  } else {
    sprite.x = tick;
    sprite.y =
      container.height -
      Math.pow(calculateExponent(endX, endY, container.height), sprite.x);
  }
};

const renderPlaneWiggle = (container, tick, sprite) => {
  sprite.y = Math.cos();
};

export const renderCrushText = (
  crashCoef,
  tick,
  coeffText,
  isCrush,
  callback
) => {
  if (isCrush) {
    callback(crashCoef);
  } else {
    coeffText.text = `${tick.toFixed(2)}`;
  }
};

export const renderBackground = (delta, speed, backgroundSprite) => {
  backgroundSprite.rotation += delta * speed;
};
