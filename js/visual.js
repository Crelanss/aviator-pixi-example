const app = window.app;

class VisualModels {
  constructor() {
    app.app.stage.on("loaded", () => {
      this.createModels();
    });
  }

  createModels() {
    const resources = app.loader.resources;
    const plane1 = resources.plane1.texture;
    const plane2 = resources.plane2.texture;
    const plane3 = resources.plane3.texture;
    const plane4 = resources.plane4.texture;
    const background = resources.background.texture;
    const propeller = resources.propeller.texture;

    Object.assign(this, {
      plane: [
        {
          texture: plane1,
        },
        {
          texture: plane2,
        },
        {
          texture: plane3,
        },
        {
          texture: plane4,
        },
      ],
      background: {
        texture: background,
      },
      propeller: {
        texture: propeller,
      },
    });
  }
}

app.visual = new VisualModels();
