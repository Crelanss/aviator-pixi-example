import * as PIXI from "pixi.js";
import plane1 from "../img/plane1.svg";
import plane2 from "../img/plane2.svg";
import plane3 from "../img/plane3.svg";
import plane4 from "../img/plane4.svg";
import background from "../img/triangleCircle.svg";
import propeller from "../img/propeller.svg";
import bgMusic from "../sounds/bg_music.mp3";
import musicSprites from "../sounds/sprite_audio.mp3";

const loader = new PIXI.Loader();
const app = window.app;
app.loader = loader;

loader
  .add("plane1", plane1)
  .add("plane2", plane2)
  .add("plane3", plane3)
  .add("plane4", plane4)
  .add("background", background)
  .add("propeller", propeller)
  .add("bgMusic", bgMusic)
  .add("musicSprites", musicSprites)
  .load(() => {
    app.app.stage.emit("loaded");
  });

loader.onError.add((e) => {
  console.log(e);
});
